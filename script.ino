#define BLINKER_PRINT    Serial
#define BLINKER_BLE

#include <Blinker.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN            5
#define NUMPIXELS      7
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define RGB_1 "RGBKey"
#define Slider1 "slider"

BlinkerSlider Slider(Slider1);
BlinkerRGB RGB1(RGB_1);


BlinkerButton light1("light1");
BlinkerButton light2("light2");
BlinkerButton light3("light3");
BlinkerButton light4("light4");
BlinkerButton light5("light5");
BlinkerButton light6("light6");
BlinkerButton light7("light7");

BlinkerButton mode1("mode1");
BlinkerButton mode2("mode2");
BlinkerButton mode3("mode3");
BlinkerButton mode4("mode4");
BlinkerButton mode5("mode5");



int tmp1 = 0;
int tmp2 = 0;
int tmp3 = 0;
int tmp4 = 0;
int tmp5 = 0;
int tmp6 = 0;
int tmp7 = 0;


void Mode1_callback(const String &a)
{
  colorWipe(pixels.Color(0,0,0), 25); // Black
  colorWipe(pixels.Color(64, 0, 0), 100); // Red
  colorWipe(pixels.Color(0, 64, 0), 100); // Green
  colorWipe(pixels.Color(0, 0, 64), 100); // Blue
}
void Mode2_callback(const String &a)
{
    colorWave(75);
  colorWipe(pixels.Color(64,0,0), 100); // red
}
void Mode3_callback(const String &a)
{
  rainbow(15);
  colorWipe(pixels.Color(64,0,0), 100); // red
}
void Mode4_callback(const String &a)
{
    rainbowCycle(15);
  colorWipe(pixels.Color(64,0,0), 100); // red
}
void Mode5_callback(const String &a)
{
 
    colorWave(30);
    colorWipe(pixels.Color(64,0,0), 100); // red
}


void Slider_callback(int32_t value)
{
  Serial.println(value);
  if(value == 0)
    RGB1.attach(rgb1_callback);
  else if(value = 1)
    RGB1.attach(rgb1_callback1);
   else if(value = 2)
    RGB1.attach(rgb1_callback2);
   else if(value = 3)
    RGB1.attach(rgb1_callback3);
   else if(value = 4)
    RGB1.attach(rgb1_callback4);
   else if(value = 5)
    RGB1.attach(rgb1_callback5);
   else if(value = 6)
    RGB1.attach(rgb1_callback6);
   else if(value = 7)
    RGB1.attach(rgb1_callback7);
    
}
void Button_callback1(const String &state){
    BLINKER_LOG("get button state: ", state);
    Serial.println(digitalRead(5));
    if(tmp1)
      { 
        
        pixels.setPixelColor(0, pixels.Color(50,50,50));
        pixels.show();
        
        tmp1 = 0;}
    else
      {     
        pixels.setPixelColor(0, pixels.Color(0,0,0));
        pixels.show();
        tmp1 = 1;
      }
  }

  void Button_callback2(const String &state){
    BLINKER_LOG("get button state: ", state);
    Serial.println(digitalRead(5));
    if(tmp2)
      { 
        
        pixels.setPixelColor(1, pixels.Color(50,50,50));
        pixels.show();
        
        tmp2 = 0;}
    else
      {     
        pixels.setPixelColor(1, pixels.Color(0,0,0));
        pixels.show();
        tmp2 = 1;
      }
  }

  void Button_callback3(const String &state){
    BLINKER_LOG("get button state: ", state);
    Serial.println(digitalRead(5));
    if(tmp3)
      { 
        
        pixels.setPixelColor(2, pixels.Color(50,50,50));
        pixels.show();
        
        tmp3 = 0;}
    else
      {     
        pixels.setPixelColor(2, pixels.Color(0,0,0));
        pixels.show();
        tmp3 = 1;
      }
  }

  void Button_callback4(const String &state){
    BLINKER_LOG("get button state: ", state);
    Serial.println(digitalRead(5));
    if(tmp4)
      { 
        
        pixels.setPixelColor(3, pixels.Color(50,50,50));
        pixels.show();
        
        tmp4 = 0;}
    else
      {     
        pixels.setPixelColor(3, pixels.Color(0,0,0));
        pixels.show();
        tmp4 = 1;
      }
  }

  void Button_callback5(const String &state){
    BLINKER_LOG("get button state: ", state);
    Serial.println(digitalRead(5));
    if(tmp5)
      { 
        
        pixels.setPixelColor(4, pixels.Color(50,50,50));
        pixels.show();
        
        tmp5 = 0;}
    else
      {     
        pixels.setPixelColor(4, pixels.Color(0,0,0));
        pixels.show();
        tmp5 = 1;
      }
  }

  void Button_callback6(const String &state){
    BLINKER_LOG("get button state: ", state);
    Serial.println(digitalRead(5));
    if(tmp6)
      { 
        
        pixels.setPixelColor(5, pixels.Color(50,50,50));
        pixels.show();
        
        tmp6 = 0;}
    else
      {     
        pixels.setPixelColor(5, pixels.Color(0,0,0));
        pixels.show();
        tmp6 = 1;
      }
  }

  void Button_callback7(const String &state){
    BLINKER_LOG("get button state: ", state);
    Serial.println(digitalRead(5));
    if(tmp7)
      { 
        
        pixels.setPixelColor(6, pixels.Color(50,50,50));
        pixels.show();
        
        tmp7 = 0;}
    else
      {     
        pixels.setPixelColor(6, pixels.Color(0,0,0));
        pixels.show();
        tmp7 = 1;
      }
  }

void rgb1_callback(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    for(int i = 0; i < NUMPIXELS; i++){
        pixels.setPixelColor(i, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    }
}
void rgb1_callback1(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    
        pixels.setPixelColor(0, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    
}
void rgb1_callback2(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    
        pixels.setPixelColor(1, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    
}
void rgb1_callback3(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    
        pixels.setPixelColor(2, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    
}
void rgb1_callback4(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    
        pixels.setPixelColor(3, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    
}
void rgb1_callback5(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    
        pixels.setPixelColor(4, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    
}
void rgb1_callback6(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    
        pixels.setPixelColor(5, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    
}
void rgb1_callback7(uint8_t r_value, uint8_t g_value, uint8_t b_value, uint8_t bright_value)
{
    //digitalWrite(5, !digitalRead(5));
    BLINKER_LOG("R value: ", r_value);
    BLINKER_LOG("G value: ", g_value);
    BLINKER_LOG("B value: ", b_value);
    BLINKER_LOG("Rrightness value: ", bright_value);

    uint8_t colorR = map(r_value, 0, 255, 0, bright_value);
    uint8_t colorG = map(g_value, 0, 255, 0, bright_value);
    uint8_t colorB = map(b_value, 0, 255, 0, bright_value);

    
        pixels.setPixelColor(6, pixels.Color(colorR,colorG,colorB));
        pixels.show();
    
}

void setup()
{
    Serial.begin(115200);

    #if defined(BLINKER_PRINT)
        BLINKER_DEBUG.stream(BLINKER_PRINT);
    #endif

    //pinMode(5, OUTPUT);
    //digitalWrite(5, LOW);

    Blinker.begin();

    pixels.begin();
    pixels.setBrightness(254);
    for(int n  = 0; n < 7; n++){
      pixels.setPixelColor(n, pixels.Color(50,50,50));
      pixels.show();
    }
    //RGB1.attach(rgb1_callback);
    Slider.attach(Slider_callback);
    light1.attach(Button_callback1);
    light2.attach(Button_callback2);
    light3.attach(Button_callback3);
    light4.attach(Button_callback4);
    light5.attach(Button_callback5);
    light6.attach(Button_callback6);
    light7.attach(Button_callback7);
  mode1.attach(Mode1_callback);
  mode2.attach(Mode2_callback);
  mode3.attach(Mode3_callback);
  mode4.attach(Mode4_callback);
  mode5.attach(Mode5_callback);
  
}

void loop()
{
    Blinker.run();

//  colorWipe(pixels.Color(0,0,0), 25); // Black
//  colorWipe(pixels.Color(64, 0, 0), 100); // Red
//  colorWipe(pixels.Color(0, 64, 0), 100); // Green
//  colorWipe(pixels.Color(0, 0, 64), 100); // Blue
  



}



// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<pixels.numPixels(); i++) {
      pixels.setPixelColor(i, c);
      pixels.show();
      delay(wait);
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<pixels.numPixels(); i++) {
      pixels.setPixelColor(i, Wheel((i+j) & 255));
    }
    pixels.show();
    delay(wait);
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< pixels.numPixels(); i++) {
      pixels.setPixelColor(i, Wheel(((i * 256 / pixels.numPixels()) + j) & 255));
    }
    pixels.show();
    delay(wait);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
   return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

/**
 *      ^   ^   ^  
 * ~~~~~ ColorWave ~~~~~
 *        V   V   V   
 */
void colorWave(uint8_t wait) {
  int i, j, stripsize, cycle;
  float ang, rsin, gsin, bsin, offset;

  static int tick = 0;
  
  stripsize = pixels.numPixels();
  cycle = stripsize * 25; // times around the circle...

  while (++tick % cycle) {
    offset = map2PI(tick);

    for (i = 0; i < stripsize; i++) {
      ang = map2PI(i) - offset;
      rsin = sin(ang);
      gsin = sin(2.0 * ang / 3.0 + map2PI(int(stripsize/6)));
      bsin = sin(4.0 * ang / 5.0 + map2PI(int(stripsize/3)));
      pixels.setPixelColor(i, pixels.Color(trigScale(rsin), trigScale(gsin), trigScale(bsin)));
    }

    pixels.show();
    delay(wait);
  }

}

/**
 * Scale a value returned from a trig function to a byte value.
 * [-1, +1] -> [0, 254] 
 * Note that we ignore the possible value of 255, for efficiency,
 * and because nobody will be able to differentiate between the
 * brightness levels of 254 and 255.
 */
byte trigScale(float val) {
  val += 1.0; // move range to [0.0, 2.0]
  val *= 127.0; // move range to [0.0, 254.0]

  return int(val) & 255;
}

/**
 * Map an integer so that [0, striplength] -> [0, 2PI]
 */
float map2PI(int i) {
  return PI*2.0*float(i) / float(pixels.numPixels());
}
