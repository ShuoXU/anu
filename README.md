# POV LED Plate

> This is a POV LED Plate made for ANU project in ENSIIE 2019

We aimed to make a POV rotating screen and at the end this POV plate could recieve the commands from a bluetooth connected mobile phone. By clicking the  buttons made on the Blinker App the player is able to control the color and the luminance of each LED.

![](https://gitlab.com/ShuoXU/anu/raw/master/image1.jpg)

## Getting Started

You should have this artcraft made by an ESP32 chip, a 3.3V battery, an old ventilator uninstalled from an old PC, and 7 LED which need to be welded together. 

### Prerequisites 

Connect the components by the given circuit diagram.

Install the Arduino IDE.
![](https://gitlab.com/ShuoXU/anu/raw/master/image2.jpg)

### Installation

First you should let your Arduino support ESP32 chip.

Windows:

```sh
1. Ctrl+comma to open up the prereferrence, add the ESP32 supporting site in the additional board supporting management: "https://dl.espressif.com/dl/package_esp32_index.json"
2. Add the ESP32 library in the library management.
3. Choose ESP32 Dev Module as the developping board in tools. 
4. Download the Blinker package."https://github.com/blinker-iot/blinker-library/archive/master.zip" Unzip the package in the My PC/My Documents/Arduino/libraries.
5. Download the IOS or Android Blinker App from "https://blinker.app/"
6. Follow the instructions of bluetooth setup. "https://blinker.app/doc/?file=001-%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B/01-arduino-%E8%93%9D%E7%89%99%E6%8E%A5%E5%85%A5"
```

### Usage example



## Reference

Most codes are referred from "https://gist.github.com/dougalcampbell/7243998"

## Authors

Ruitong XU
Jiahui XU
Shuo XU
